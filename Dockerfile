FROM ubuntu:20.04

LABEL Alexander Drummer <drummerroma@gmail.com>

ENV URL_FQDN lists.example.com
ENV EMAIL_FQDN lists.example.com
ENV MASTER_PASSWORD example
ENV LIST_LANGUAGE_CODE en
ENV LIST_LANGUAGE_NAME English
ENV LIST_ADMIN admin@lists.example.com
ENV DEBUG_CONTAINER false
ENV DKIM_SELECTOR mailinglist

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y mailman apache2 postfix-policyd-spf-python opendkim opendkim-tools rsyslog supervisor tcpdump telnet

RUN adduser postfix opendkim

COPY mailman.conf /etc/apache2/sites-available/

# postfix https://github.com/zooniverse/docker-mailman/blob/master/Dockerfile
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY logrotate.conf /etc/logrotate.d/mailman
COPY start.sh /usr/local/bin/start.sh
COPY rsyslog-hup.sh /etc/cron.daily/zzz-rsylog-hup
COPY mailman-restart.sh /etc/cron.hourly/mailman-restart

RUN printf '#!/bin/sh\nexit 0' > /usr/sbin/policy-rc.d
RUN printf '#!/bin/sh\necho 1' > /sbin/runlevel
RUN chmod +x /usr/local/bin/start.sh /sbin/runlevel
# postfix

COPY mailman-config.cfg /


VOLUME /var/log/mailman
VOLUME /var/log/
VOLUME /var/log/apache2
VOLUME /var/lib/mailman/archives
VOLUME /var/lib/mailman/lists
VOLUME /etc/dkimkeys
VOLUME /etc/postfix/certs


EXPOSE 25 80

ENTRYPOINT [ "/usr/local/bin/start.sh" ]
