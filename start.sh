#!/bin/bash
if [ $DEBUG_CONTAINER == 'true' ]; then
	outfile='/dev/console'
else
	outfile='/dev/null'
fi
mailmancfg='/etc/mailman/mm_cfg.py'

cat << EOB

    **********************************************************
    *                                                        *
    *   Docker image: drummerroma/mailman2                   *
    *   https://gitlab.mpcdf.mpg.de/adrum/docker-mailman2    *
    *                                                        * 
    **********************************************************

EOB

echo -n "Set rights on rundir..."
{
	groupadd list
	mkdir /var/run/mailman
	chown list:list /var/run/mailman
} &>$outfile
echo ' Done.'


################### START DKIM #################

echo -n "Setting up DKIM..."
{
	if [ ! -f /etc/dkimkeys/${DKIM_SELECTOR}.private ]; then
        opendkim-genkey -t -D /etc/dkimkeys -d ${EMAIL_FQDN} -s ${DKIM_SELECTOR} -b 1024
		sleep 5
        chmod 600 /etc/dkimkeys/${DKIM_SELECTOR}.private
        chown opendkim:opendkim /etc/dkimkeys/${DKIM_SELECTOR}.private
	fi
} &>$outfile
echo ' Done.'

if [ -f /etc/dkimkeys/${DKIM_SELECTOR}.private ]; then
	chmod 600 /etc/dkimkeys/${DKIM_SELECTOR}.private
	chown opendkim:opendkim /etc/dkimkeys/${DKIM_SELECTOR}.private
fi
gpasswd -a postfix opendkim

# /etc/postfix/main.cf

postconf -e milter_protocol=6
postconf -e milter_default_action=accept
postconf -e smtpd_milters=local:/opendkim/opendkim.sock
postconf -e non_smtpd_milters=local:/opendkim/opendkim.sock

# empty opendkim
cat > /etc/opendkim.cfg
cat >> /etc/opendkim.conf <<EOF
AutoRestart             Yes
AutoRestartRate         10/1h
UMask                   002
Syslog                  yes
SyslogSuccess           Yes
LogWhy                  Yes
Canonicalization        relaxed/simple
Mode                    sv
SignatureAlgorithm      rsa-sha256
UserID                  opendkim:opendkim
Socket                  local:/var/spool/postfix/opendkim/opendkim.sock
DNSTimeout              5
OversignHeaders         From
EOF

echo "Domain ${EMAIL_FQDN}" >> /etc/opendkim.conf
echo "KeyFile /etc/dkimkeys/${DKIM_SELECTOR}.private" >> /etc/opendkim.conf
echo "Selector ${DKIM_SELECTOR}" >> /etc/opendkim.conf

/bin/sed -i "s/PIDFile=\/run\/opendkim\/opendkim.pid/#PIDFILE/" /lib/systemd/system/opendkim.service

/bin/sed -i "s/#RUNDIR=\/var\/spool\/postfix\/run\/opendkim/RUNDIR=\/var\/spool\/postfix\/opendkim/" /etc/default/opendkim
/bin/sed -i "s/RUNDIR=\/run\/opendkim/#/" /etc/default/opendkim

################### END DKIM #################


################### POSTFIX ###############
############## from https://github.com/catatnight/docker-postfix/blob/master/assets/install.sh
# main.cf
# postfix main.cfg
postconf -e "myhostname=${EMAIL_FQDN}"

postconf -e "alias_maps= hash:/etc/aliases, hash:/var/lib/mailman/data/aliases"
postconf -e "mydomain=${EMAIL_FQDN}"
postconf -e "myorigin=${EMAIL_FQDN}"
postconf -e "mynetworks_style = host"
postconf -e smtpd_recipient_restrictions=permit_mynetworks, permit_sasl_authenticated,reject_unauth_destination,check_policy_service unix:private/policyd-spf
postconf -e policyd-spf_time_limit=3600

# without tls
postconf -e "smtp_tls_security_level=none"


# SPF
# https://rigacci.org/wiki/doku.php/doc/appunti/linux/sa/postfix_spf_check
# add spf check
echo "policyd-spf  unix  -       n       n       -       0       spawn" >> /etc/postfix/master.cf
echo "  user=policyd-spf argv=/usr/bin/policyd-spf" >> /etc/postfix/master.cf


###########
# Enable TLS
###########
#if [[ -n "$(find /etc/postfix/certs -iname *.crt)" && -n "$(find /etc/postfix/certs -iname *.key)" ]]; then
if [[ -n "$(find /etc/postfix/certs -iname *.pem)" ]]; then
# /etc/postfix/main.cf
  postconf -e "tls_medium_cipherlist = ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA256:AES256-SHA256:AES256-GCM-SHA384"

#  postconf -e "smtp_tls_security_level=may"

  postconf -e smtpd_tls_cert_file=$(find /etc/postfix/certs -iname *.pem)
  postconf -e smtpd_tls_key_file=$(find /etc/postfix/certs -iname *.pem)

  postconf -e smtp_tls_key_file = /etc/postfix/certs  -iname *.pem
  postconf -e smtp_tls_cert_file = /etc/postfix/certs  -iname *.pem

  chmod 400 /etc/postfix/certs/*.*
  # /etc/postfix/master.cf
#  postconf -M submission/inet="submission   inet   n   -   n   -   -   smtpd"
#  postconf -P "submission/inet/syslog_name=postfix/submission"
#  postconf -P "submission/inet/smtpd_tls_security_level=encrypt"
#  postconf -P "submission/inet/smtpd_sasl_auth_enable=yes"
#  postconf -P "submission/inet/milter_macro_daemon_name=ORIGINATING"
#  postconf -P "submission/inet/smtpd_recipient_restrictions=permit_sasl_authenticated,reject_unauth_destination"

# /etc/postfix/master.cf
#/bin/sed -i "s/#tlsproxy\  unix\  -\       -\       y\       -\       0\       tlsproxy/tlsproxy\  unix\  -\       -\       y\       -\       0\       tlsproxy/" /etc/postfix/master.cf
fi



################### MAILMAN ###############

# Add some directives to Mailman config:
echo 'MTA = "Postfix"' >> $mailmancfg
echo 'DELIVERY_MODULE = "SMTPDirect"' >> $mailmancfg
echo 'SMTP_MAX_RCPTS = 500' >> $mailmancfg
echo 'MAX_DELIVERY_THREADS = 0' >> $mailmancfg
echo 'SMTPHOST = "localhost"' >> $mailmancfg
echo 'SMTPPORT = 0' >> $mailmancfg

# Fill debconf files with proper runtime values:
if [ $LIST_LANGUAGE_CODE != "en" ]; then
	/bin/sed -i "s/default_server_language\ select\ en\ (English)/default_server_language\ select\ ${LIST_LANGUAGE_CODE}\ (${LIST_LANGUAGE_NAME})/" /mailman-config.cfg
	/bin/sed -i "/^mailman mailman\/site_languages/ s/$/\,\ ${LIST_LANGUAGE_CODE}\ \(${LIST_LANGUAGE_NAME}\)/" /mailman-config.cfg
fi

# Replace default hostnames with runtime values:
/bin/sed -i "s/lists\.example\.com/${URL_FQDN}/" /etc/apache2/sites-available/mailman.conf
/bin/sed -i "s/DEFAULT_EMAIL_HOST.*\=.*/DEFAULT_EMAIL_HOST\ \=\ \'${EMAIL_FQDN}\'/" $mailmancfg
/bin/sed -i "s/DEFAULT_URL_HOST.*\=.*/DEFAULT_URL_HOST\ \=\ \'${URL_FQDN}\'/" $mailmancfg
/bin/sed -i "s/DEFAULT_SERVER_LANGUAGE.*\=.*/DEFAULT_SERVER_LANGUAGE\ \=\ \'${LIST_LANGUAGE_CODE}\'/" $mailmancfg

# master - remove ubuntu default python script
/bin/sed -i "s/mailman\   unix\  -\       n\       n\       -\       -\       pipe/#/" /etc/postfix/master.cf
/bin/sed -i "s/\  flags=FR\ user=list\ argv=\/usr\/lib\/mailman\/bin\/postfix-to-mailman.py/#/" /etc/postfix/master.cf
/bin/sed -i "s/\  \${nexthop}\ \${user}//" /etc/postfix/master.cf


# remove mm_cfg.pyc, to ensure the new values are picked up
rm -f "${mailmancfg}c"
rm -f "/var/lib/mailman/Mailman/mm_cfg.pyc"

echo -n "Mailman master password..."
{
	/usr/sbin/mmsitepass ${MASTER_PASSWORD}
} &>$outfile
echo ' Done.'


echo -n "Generate default mailman list if not exists.."
{
if [ ! -d /var/lib/mailman/lists/mailman ]; then
	chown -R list:list /var/lib/mailman
	/usr/sbin/newlist -q -l ${LIST_LANGUAGE_CODE} mailman ${LIST_ADMIN} ${MASTER_PASSWORD}
fi
}
echo ' Done.'

# Addaliases and update them:
cat << EOA >> /etc/aliases
mailman:              "|/var/lib/mailman/mail/mailman post mailman"
mailman-admin:        "|/var/lib/mailman/mail/mailman admin mailman"
mailman-bounces:      "|/var/lib/mailman/mail/mailman bounces mailman"
mailman-confirm:      "|/var/lib/mailman/mail/mailman confirm mailman"
mailman-join:         "|/var/lib/mailman/mail/mailman join mailman"
mailman-leave:        "|/var/lib/mailman/mail/mailman leave mailman"
mailman-owner:        "|/var/lib/mailman/mail/mailman owner mailman"
mailman-request:      "|/var/lib/mailman/mail/mailman request mailman"
mailman-subscribe:    "|/var/lib/mailman/mail/mailman subscribe mailman"
mailman-unsubscribe:  "|/var/lib/mailman/mail/mailman unsubscribe mailman"
EOA

chown root:root /etc/aliases
/usr/bin/newaliases
################### END MAILMAN ###############



echo -n "Setting up Apache web server..."
{
	a2enmod cgi
	a2ensite mailman.conf
} &>$outfile

echo -n "Fixing permissons and finishing setup..."
{
	#chown -R list:list /var/lib/mailman/
	/usr/lib/mailman/bin/check_perms -f
    /usr/lib/mailman/bin/genaliases
} &>$outfile
echo ' Done.'


mkdir -p /var/run/mailman
chmod 777 /var/run/mailman

exec /usr/bin/supervisord